import * as React from 'react';
import { Text, View, StyleSheet, ScrollView, TextInput } from 'react-native';
import { Constants } from 'expo';

// You can import from local files
import AssetExample from './components/AssetExample';

// or any pure javascript modules available in npm
var search ='';
const url2 = 'http://ecatalog.kiev.ua/F/SJGR95E9G1GEXFV14V6QQ3PAB2QRTG9NBQSJULYCD5HKLL9YHK-00731?func=find-b&request='+search+'&find_code=WRD&adjacent=N&x=57&y=14';

export default class App extends React.Component {
  state = {
    text: ''
  }

  async componentDidMount() {
    console.log('did mount, fetching: ' + url2)
    try {
      let response = await fetch(url2);
      let text = await response.text();
      console.log(text)
      this.setState({ text })
    } catch(e) {
      console.log(e)
    }
  }

render() {
    return (
      <ScrollView>
       <View style={styles.container}>
        <Text style={styles.paragraph}>
          Text: {this.state.text}
        </Text>
        <View style={{padding: 10}}>
        <TextInput
          style={{height: 40}}
          placeholder="Type here to translate!"
          onChangeText={(text) => this.setState({text})}
        />
        <Text style={{padding: 10, fontSize: 42}}>
          {this.state.text.split(' ').map((word) => word && '🍕').join(' ')}
        </Text>
      </View>
      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
});
