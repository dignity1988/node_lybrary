const mongoose = require('mongoose');
var Users= new mongoose.Schema({
	    abonement_id: {type:Number, required:false, trim:true,unique:true},
      name: {type:String, required:false, trim:true,unique:false},
			surname: {type:String, required:false, trim:true,unique:false},
			lastname: {type:String, required:false, trim:true,unique:false},
	    phone_number:  {type:String, required:false, trim:true,unique:false},
      address:  {type:String, required:false, trim:true,unique:false},
      date_of_birth: {type:String, required:false, trim:true,unique:false},
      occupation: {type:String, required:false, trim:true,unique:false},
      registration_year: {type:String, required:false, trim:true,unique:false},
      notes: {type:String, required:false, trim:true,unique:false},
			education: {type:String, required:false, trim:true,unique:false},
			passport: {type:String, required:false, trim:true,unique:false},
			date_of_visit: {type:Object, required:false, trim:true,unique:false},
    })
var user = mongoose.model('user', Books);
/*testing part save from mongoose
    var user4= new user({
      abonement_id: '00000300000112221',
      name:'Maksym',
      surname:'Malin',
      phone_number: '000000000000000',
      address: '0000',
      date_of_birth:'00000',
      occupation: '00000',
      registration_year: '000000',
  notes:'good dude'
});
user4.save(function (err, user) {
if (err) return console.error(err);
console.log('saved',user);
})
*/
// save model to database
user.find(function (err, user) {
if (err) return console.error(err);
console.log(user);
})

module.exports = mongoose.model('users', Users);
