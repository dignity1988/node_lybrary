
const User = require('./user.model.js');
exports.create = (req, res) => {
// Request validation
if(!req.body) {
return res.status(400).send({
message: "user content can not be empty"
});
}
const user = new User({
        abonement_id: req.body.abonement_id || "No user title",
        surname: req.body.surname,
        lastname: req.body.lastname,
        name: req.body.name,
        phone_number: req.body.phone_number,
        address: req.body.address,
        date_of_birth: req.body.date_of_birth,
        occupation:  req.body.occupation,
        registration_year: req.body.registration_year,
        notes: req.body.notes,
        education:  req.body.education,
        passport:req.body.passport,
        visit: req.body.visit,
});
 // Save user in the database
    user.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Щось пішло не так на сервері під час збереження нового користувача"
        });
    })
      User.find()
      .then(users => {
          res.send(users);
      }).catch(err => {
          res.status(500).send({
              message: err.message || "Щось пішло не так підчас оновлення списку користувачів"
          });
      })
}
// Retrieve all users from the database.
exports.findAll = (req, res) => {
    User.find()
    .then(users => {
        res.send(users);//console.log('!!!!!!!!!!!'+users);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Щось пішло не так підчас оновлення списку користувачів"
        });
    });
};
// Update a user
exports.update = (req, res) => {
    // Validate Request
    if(!req.body) {
        return res.status(400).send({
            message: "Заповніть необхідні поля будь ласка"
        });
    }

    // Find and update user with the request body
    User.findByIdAndUpdate(req.body.abonement_id, {
      abonement_id: req.body.abonement_id || "Не вказано абонентського номеру",
      surname: req.body.surname || "Не вказано призвіще",
      name: req.body.name || "Не вказно ім'я",
      lastname: req.body.name || "Не вказано призвіще",
      phone_number: req.body.phone_number || "Не вказано номер телефону",
      address: req.body.address || "Не вказано адресу",
      date_of_birth: req.body.date_of_birth || "Не вказана дата народження",
      occupation:  req.body.occupation || "Не вказана професія",
      education:  req.body.education || "Не вказана професія",
      occupation:  req.body.occupation || "Не вказана професія",
      registration_year: req.body.registration_year || "Не вказан рік реєстрації",
      notes: req.body.notes || "немає записів",
      education:  req.body.education,
			passport:req.body.passport,
			visit: req.body.visit,
    }, {new: true})
    .then(user => {
        if(!user) {
            return res.status(404).send({
                message: "Не знайдено користувача з абонентським номером " + req.body.abonement_id
            });
        }
        res.send(user);
    }).catch(err => {
        if(err.kind === 'abonement_id') {
            return res.status(404).send({
                message: "Користувач с абонементським номером не знайдено" + req.body.abonement_id
            });
        }
        return res.status(500).send({
            message: "Щось пошло не так під час спроби оновити інформацію про користувача" + req.body.abonement_id
        });
    });
};

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    User.findByIdAndRemove(req.body.abonement_id)
    .then(user => {
        if(!user) {
            return res.status(404).send({
                message: "Не знайдено користувача за таким абонентським номером" + req.body.abonement_id
            });
        }
        res.send({message: "Користувача успішно видалено!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Product not found with id " + req.body.abonement_id
            });
        }
        return res.status(500).send({
            message: "Не можу видалити користувача з номером абонементу" + req.body.abonement_id
        });
})
}



/*PhishingDB.update({url: urlReq},{$push: {ips:{**ip**:tmpIp}}},{upsert:true},function(err){
            if(err){
                    console.log(err);
            }else{
                    console.log("Successfully added");
            }
    });
    BusUnit.findOne({uniqueId: 'some old existing doc id'}, function (err, doc) {
        if (err) return done(err);
        // Create the new field if it doesn't exist yet
        doc.workUnits || (doc.workUnits = [];)
        doc.workUnits.push('value');

        doc.save(done);
    });





    */
