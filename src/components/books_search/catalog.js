import React from 'react';
import PropTypes from 'prop-types';
import {
  withStyles
} from '@material-ui/core/styles';

import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import SearchIcon from '@material-ui/icons/Search';
import axios from 'axios';
import Avatar from '@material-ui/core/Avatar';
import  IconButton  from '@material-ui/core/IconButton';

import CardMedia from '@material-ui/core/CardMedia';
import CardHeader from '@material-ui/core/CardHeader';

import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Card from '@material-ui/core/Card';
import Collapse from '@material-ui/core/Collapse';



import red from '@material-ui/core/colors/red';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import Typography from '@material-ui/core/Typography';
const styles = theme => ({
  root: {
    margin: theme.spacing.unit * 3,

  },
  book: {
    padding: 50 + 'px',
    display: 'flex',
    alignItems: 'stretch',
  },
  img:{
    width:50,
    height:50,
    margin:'Auto',
  },
  card:{
    width:200,
    height:200,
  },

  table: {
    flex: 1
  },
  row: {
    marginLeft: -12,
    marginLeft: 20
  },
  card: {
    minWidth: 275,
    alignSelf: 'stretch',
    padding: '20px',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
})
const numberOfTries =3;
let word = 'Тютчев';


class Catalog extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        word: 'лем',
        books: [],
        error: false,
        isLoading: 'before',
      }
      this.handleChange = this.handleChange.bind(this)
      this.handleKeyPress = this.handleKeyPress.bind(this)
      this.bookes = this.bookes.bind(this)
    }
    /*
    componentDidMount() {
      fetch('http://localhost:5000/search_books/?term=' + 'фет', {
          'Cache-Control': 'no-cache, no-store, must-revalidate',
          'Pragma': 'no-cache',
          'cache-control': 'no-cache',
          'Expires': 0
        })
        .then(response => {
          if (response && response.ok && response.status == 200) return response.json()
        }).then(json => this.setState({
          books: json
        })).then(console.log(this))
        .then(data => this.setState({
          isLoading: false
        }))
        .catch(error => this.setState({
          error,
          isLoading: true
        }));
    }

*/
/*error in fetching resource*/
    handleChange(e) {
      let books = [];
      this.setState({
        [e.target.name]: e.target.value
      });
      /* var self = this;*/
      var val = e.target.value;
      this.setState({
        books: []
      });
      this.setState({
        word: val
      });
    }

           handleKeyPress = async e => {
            if (e.key === 'Enter') {
              var self = this;
              var val = (e.target.value).toLowerCase();
              console.log('enter');
              this.setState({
                [e.target.name]: e.target.value
              });
              this.setState({
                word: val,
                isLoading: 'loading',
              });
              let  currentTry = 1;
       await fetch('/search_books/wrd/?term=' + val, {method: "GET",
      headers: {
          "content-type": "application/json",
           'Cache-Control': 'no-cache, no-store, must-revalidate',
                    'Pragma': 'no-cache',
                   'cache-control': 'no-cache'
        },
      }
                )
                .then(response => {
                  if (response.ok && response.status === 200) return response.json()
                }).then(json => this.setState({
                  books: json,   isLoading: 'false'
                }))
              .catch(function(error) {
console.log(error);
this.setState({
isLoading: 'error', books:'' })
              })


              }
            }

    /*
    {output.map(key => (



    <ul className="book_card">


      <p>{key.id}</p>
      <p>{key.system_id}</p>
      <p>{key.author}</p>
      <p>{key.Name}</p>
      <p>{key.edition}</p>
      <p>{key.ISBN}</p>
      <p>{key.author_sign}</p>
      <p>{key.shelf_id}</p>
      <p>{key.publisher}</p>
      <p>{key.publish_year}</p>
      <p>{key.pages}</p>
      <p>{key.series}</p>
      <p>{key.rubric}</p>
      <p>{key.type}</p>
    </ul>
    */

    /*if books exists then use diferent isFethcing true. If is fetching true render the icon of fetching centered div*/
    /*if eror then (request,response) => {reqeust = some()=>{fetch one more time}}*/
    /*if request */


    bookes() {
        const {classes
        } = this.props;
        var self = this;

        let val=val;
        var json = this.state.books;
      console.log(this.state.isLoading);
      console.log(json.length);
      if ((!this.state.isLoading) && (json.length<=0))  {
        return ( <div>Введить пошуковий запит</div>)

      }
      else if ((this.state.isLoading =='false' ) && (json.length >0))  {
          return ( <div className="books_params"> {
              json.map(key => (
    <Card className={ classes.card }>
    <CardHeader
        avatar={
          <Avatar aria-label="Recipe" className={classes.avatar}>
            R
          </Avatar>
        }
        action={
          <IconButton>
            <MoreVertIcon />
          </IconButton>
        }
        title= {
          key.books_name.author
          }
        subheader= {
          key.books_name.name
          }
      />
      <CardMedia

       image={
         key.books_name.thumbnail
         }
       title="Э у нас"
     />
      <CardContent>
        <Typography className={ classes.title } color="textSecondary" gutterBottom>
         </Typography>
         {key.books_name.name
           }
        <Typography className={ classes.pos } color="textSecondary">
          </Typography> <Typography component="p">
          <p> {
            key.books_name.rubric
            } </p>
          <p> {
            key.books_name.author
            } </p>



              <p> {
                key.books_name.publisher
                } </p>
                <p> {
                  key.books_name.thumbnail
                  } </p>





            </Typography> </CardContent>
              <CardActions>
                <Button size="small"> Замовити </Button> </CardActions>
                    </Card> )) } </div> )
                  }
                      else if ((this.state.isLoading =='error'))
                        {
return ( <div>Повторіть пошук. За пошуковим запитом {val}</div>)
}
else if ( ((this.state.isLoading ='before'))) {
return ( <div> Для пошуку за автором введіть призвіще автора у пошуковому полі </div>)  }
else if ( this.state.isLoading == 'loading' )
                        { return ( <div> loading </div>)  }

                }
                       render() { const { classes }=this.props; return ( <div>
                          <Paper className={ classes.container } noValidate autoComplete="off">
                            <TextField value={ this.state.search } onKeyPress={ this.handleKeyPress } autoComplete="onn" id="outlined-name" label="ПОШУК ЗА АВТОРОМ" name="search" className={ classes.textField } onChange={ this.handleChange }
                              defaultValue={ this.state.search } margin="normal" variant="outlined" />
                              <div className={ classes.searchIcon }>
                                <SearchIcon />
                                  </div> </Paper>
                                    <Paper className={ classes.book }> {
                                      this.bookes()
                                      } </Paper> </div> )
                                        }
                                        }
                                        Catalog.propTypes = {
                                        classes: PropTypes.object.isRequired,
                                         }
                                        export default withStyles(styles)(Catalog);
