import React, { Component } from "react";
import { withRouter } from "react-router";

import firebase, {users} from "../../components/firebase";
import SignUpView from "./SignUpView";

class SignUp extends Component {

  handleSignUp = async event => {
    event.preventDefault();

    const { email, password } = event.target.elements;
    try {
      const user = await firebase.auth().createUserWithEmailAndPassword(email.value, password.value).then(function(user){firebase.database().ref('users/' + user.userId).set({

          email: user.email,

        });
},function(err){
     console.log(err);
});

      this.props.history.push("/");
    } catch (error) {
      alert(error);
    }
  };

  render() {
    return <SignUpView onSubmit={this.handleSignUp} />;
  }
}

export default withRouter(SignUp);
