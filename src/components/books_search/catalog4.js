import React from 'react';
import PropTypes from 'prop-types';
import {
  withStyles
} from '@material-ui/core/styles';

import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import SearchIcon from '@material-ui/icons/Search';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';

import Typography from '@material-ui/core/Typography';
const styles = theme => ({
  root: {
    margin: theme.spacing.unit * 3,

  },
  book: {
    padding: 50 + 'px',
    display: 'flex',
    alignItems: 'stretch',
  },
  table: {
    flex: 1
  },
  row: {
    marginLeft: -12,
    marginLeft: 20
  },
  card: {
    minWidth: 275,
    alignSelf: 'stretch',
    padding: '20px',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
})
const numberOfTries =3;
let word = 'Тютчев';
function getjson(val, currentTry){
fetch('http://localhost:5000/search_books/wrd/?term=' + val, {
    'Cache-Control': 'no-cache, no-store, must-revalidate',
    'Pragma': 'no-cache',
    'cache-control': 'no-cache',
    'Expires': 0
  })
  .then(response => {
    if (response.ok && response.status === 200) return response.json()
  }).then(json => this.setState({
    books: json
  }))
  .then(data => this.setState({
    isLoading: false
  })).catch(function() {

  if (currentTry >= numberOfTries) return  getjson(val, currentTry + 1).then(response => {
      if ( response.ok && response.status === 200) return response.json()
    }).then(json => this.setState({
      books: json
    }))
    .then(response => this.setState({
      isLoading: false
    }))
    .catch(error => {
      this.setState({
        error: error,
        isLoading: false,
      });
    });
});
}
class Catalog extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        word: 'Шевченко',
        books: [],
        error: false,
        isLoading: false
      }
      this.handleChange = this.handleChange.bind(this)
      this.handleKeyPress = this.handleKeyPress.bind(this)
      this.bookes = this.bookes.bind(this)
    }
    /*
    componentDidMount() {
      fetch('http://localhost:5000/search_books/?term=' + 'фет', {
          'Cache-Control': 'no-cache, no-store, must-revalidate',
          'Pragma': 'no-cache',
          'cache-control': 'no-cache',
          'Expires': 0
        })
        .then(response => {
          if (response && response.ok && response.status == 200) return response.json()
        }).then(json => this.setState({
          books: json
        })).then(console.log(this))
        .then(data => this.setState({
          isLoading: false
        }))
        .catch(error => this.setState({
          error,
          isLoading: true
        }));
    }

*/

    handleChange(e) {
      let books = [];
      this.setState({
        [e.target.name]: e.target.value
      });
      /* var self = this;*/
      var val = e.target.value;
      this.setState({
        books: []
      });
      this.setState({
        word: val
      });
    }



          handleKeyPress = e => {
            if (e.key === 'Enter') {
              var self = this;
              var val = (e.target.value).toLowerCase();
              console.log('enter');
              this.setState({
                [e.target.name]: e.target.value
              });
              this.setState({
                word: val
              });
              this.setState({
                isLoading: 'true'
              })
              getjson(val, 3);

              }
            }

    /*
    {output.map(key => (



    <ul className="book_card">


      <p>{key.id}</p>
      <p>{key.system_id}</p>
      <p>{key.author}</p>
      <p>{key.Name}</p>
      <p>{key.edition}</p>
      <p>{key.ISBN}</p>
      <p>{key.author_sign}</p>
      <p>{key.shelf_id}</p>
      <p>{key.publisher}</p>
      <p>{key.publish_year}</p>
      <p>{key.pages}</p>
      <p>{key.series}</p>
      <p>{key.rubric}</p>
      <p>{key.type}</p>
    </ul>
    */

    /*if books exists then use diferent isFethcing true. If is fetching true render the icon of fetching centered div*/
    /*if eror then (request,response) => {reqeust = some()=>{fetch one more time}}*/
    /*if request */


    bookes() {
        const {
          classes
        } = this.props;
        var self = this;
        var json = this.state.books;
        if ((!this.state.isLoading) ) {
          return ( < div className = "books_params" > {
              Object.keys(json).map((d) =>
                <
                Card className = {
                  classes.card
                } >
                <
                CardContent >
                <
                Typography className = {
                  classes.title
                }
                color = "textSecondary"
                gutterBottom >
                <
                p > {
                  d.toString()
                } < /p> < /Typography > < Typography variant = "h5"
                component = "h2" >
                <
                p > {
                  d
                } < /p> < /Typography > < Typography className = {
                  classes.pos
                }
                color = "textSecondary" >

                <
                /Typography> < Typography component="p">


                <
                /Typography> < /CardContent > < CardActions >
                <
                Button size = "small" > Замовити < /Button> < /CardActions > < /Card> ) } < /div > )
            }
            else {
              return ( < div > loading < /div>) } } render() { const { classes }=this.props; return ( < div> <
                Paper className = {
                  classes.container
                }
                noValidate autoComplete = "off" >
                <
                TextField value = {
                  this.state.search
                }
                onKeyPress = {
                  this.handleKeyPress
                }
                autoComplete = "onn"
                id = "outlined-name"
                label = "ПОШУК ЗА АВТОРОМ"
                name = "search"
                className = {
                  classes.textField
                }
                onChange = {
                  this.handleChange
                }
                defaultValue = {
                  this.state.search
                }
                margin = "normal"
                variant = "outlined" / >
                <
                div className = {
                  classes.searchIcon
                } >
                <
                SearchIcon / >
                <
                /div> < /Paper > < Paper className = {
                  classes.book
                } > {
                  this.bookes()
                } < /Paper> < /div > )
            }
          }
          Catalog.propTypes = {
            classes: PropTypes.object.isRequired,
  }
          export default withStyles(styles)(Catalog);
