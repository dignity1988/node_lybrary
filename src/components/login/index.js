import React, { Component } from "react";
import { withRouter } from "react-router";
import firebase, {auth, provider} from "../../components/firebase";
import LoginView from "./loginView";
class Login extends Component {
  handlelogin = async event => {
    event.preventDefault();
    const { email, password } = event.target.elements;
    try {
      const user = await auth.signInWithEmailAndPassword(email.value, password.value);
      this.props.history.push("/");
    } catch (error) {
      alert(error);
    }
  };
  render() {
    return <LoginView onSubmit={this.handlelogin} />;
  }
}

export default withRouter(Login);
