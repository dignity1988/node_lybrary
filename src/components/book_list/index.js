import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import SearchInput, {createFilter} from 'react-search-input';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
const Dom = require('xmldom').DOMParser;
const Select = require('xpath.js');









const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    border: theme.palette.common.green,
    padding: 5+'px',
    textAlign: 'left',
  },
  body: {
    fontSize: 14,
      textAlign: 'left',
    padding: 5+'px',
  },
}))(TableCell);

const styles = theme => ({
  root: {
    margin: theme.spacing.unit *3,

  },
table: {
    flex: 1
  },
row: {
    marginLeft: -12,
    marginLeft: 20
  }
})




const KEYS_TO_FILTERS = ['abonement_id','surname', 'name'];
var startDate = new Date("2019-01-01"); // YYYY-MM-DD
var endDate = new Date("2020-01-01"); //YYYY-MM-DD
/*
var getDateArray = function(start, end) {
    var arr = new Array();  // arr = [];
    var dt = new Date(start);
    // map()
    while (dt <= end ) {
        if (dt.getDay() != 5) {
          arr.push(new Date(dt));
        }
        dt.setDate(dt.getDate() + 1);
    }
    return arr;
}


 getDateArray();

var dateArr=getDateArray(startDate, endDate);

function DrawCells(dateArr){
for (let i = 0; i < dateArr.length; i++) {
return(
  <CustomTableCell>
{dateArr[i].getDay()}{dateArr[i].getFullMonth()}
  </CustomTableCell>
);
}
}
*/
const searchresults = () =>{
  fetch( )
  .then(response => {
  if(response.ok) return response.json()}
  ).then(json => this.setState({users:json})).then(console.log(this.state.users))
        .then(data => this.setState({ hits: data.hits, isLoading: false }))
      .catch(error => this.setState({ error, isLoading: false }));
  }

}
class Books extends React.Component {
  constructor (props) {
  super(props)
  this.state = {
  }
   this.searchUpdated = this.searchUpdated.bind(this)
  }
componentDidMount = () => this.fetchData();
  fetchData = async () => {
    try {
      const xml = fetch( 'http://212.1.70.66/X?op=find&request=wau=%22%D0%BF%D0%B5%D0%BB%D0%B5%D0%B2%D0%B8%D0%BD%22&base=sbl033', {
     method: 'GET',
     credentials: 'include',

    }).then(function(response){
      return response;
    }).then(function(response){
  return response.text();
    }).then(function(text){
  console.log(text);
  return text;
    }).catch(function(err){
      console.log(err);
    });

const Data = await productRes.json(); // convert productRes to JSON




var doc = new Dom().parseFromString(xml);
var setnumber = xpath.select("string(//doc_number)", doc);
books=array.push(setnumber);




}

/*get the data of each book*/


      const storeIDs = productData.map(({store_id}) => (store_id)); // map over productData JSON for "store_ids" and store them into storeIDs

      const storeData = [];
      await Promise.each(storeIDs, async id => { // loop over "store_ids" inside storeIDs
        try {
          const storeRes = await fetch(`myapi.com/store?id=${id}`); // fetch data by "id"
          const storeJSON = await storeRes.json(); // convert storeRes to JSON
          storeData.push(storeJSON); // push storeJSON into the storeData array, then loop back to the top until all ids have been fetched
        } catch(err) { console.error(err) }
      });

      this.setState({ productData, storeData }) // set both results to state
    } catch (err) {
      console.error(err);
    }
  }


render(){
 const {classes} = this.props;
 /*creating several diferent filters that works separately*/
 const filteredNames = this.state.users.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
 const {dateArr} = this.props;
  return (
    <div>
    <SearchInput placeholder="Номер абонементу" className="search-input" onChange={this.searchUpdated} />


      <Table  className={this.props.classes.table}>
        <TableHead>
          <TableRow  className={this.props.classes.row}>
            <CustomTableCell>#№</CustomTableCell>
            <CustomTableCell  align="left">Им'я</CustomTableCell>
            <CustomTableCell align="left">Призвіще</CustomTableCell>
            <CustomTableCell align="left">По-батькові</CustomTableCell>
            <CustomTableCell align="left">Рік народження</CustomTableCell>
          <CustomTableCell align="left">Освіта</CustomTableCell>
            <CustomTableCell align="left">Пофесія</CustomTableCell>
            <CustomTableCell align="left">Місце роботи</CustomTableCell>
          <CustomTableCell align="left">Домашня адреса</CustomTableCell>
        <CustomTableCell align="left">Телефон</CustomTableCell>
          <CustomTableCell align="left">Рік реєстрації</CustomTableCell>
      <CustomTableCell align="left">Останній візит</CustomTableCell>
          </TableRow>
        </TableHead>
        <TableBody>

          {filteredNames.map(key => (
            <TableRow>
              <CustomTableCell component="th" scope="row">
                {key.name}
              </CustomTableCell>
              <CustomTableCell align="left">{key.abonement_id}</CustomTableCell>
              <CustomTableCell align="left">{key.name}</CustomTableCell>
<CustomTableCell align="left">{key.surname}</CustomTableCell>
<CustomTableCell align="left">{key.lastname}</CustomTableCell>
<CustomTableCell align="left">{key.date_of_birth}</CustomTableCell>
<CustomTableCell align="left">{key.education}</CustomTableCell>
<CustomTableCell align="left">{key.occupation}</CustomTableCell>
<CustomTableCell align="left">{key.address}</CustomTableCell>
<CustomTableCell align="left">{key.phone_number}</CustomTableCell>
<CustomTableCell align="left">{key.registration_year}</CustomTableCell>
<CustomTableCell align="left">{key.visit}</CustomTableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>

          <Table  className={this.props.classes.table}>
            <TableHead>
            <TableRow  className={this.props.classes.row}>
            <CustomTableCell>#№</CustomTableCell>
            <CustomTableCell align="left">10:00</CustomTableCell>
            <CustomTableCell align="left">11:00</CustomTableCell>
            <CustomTableCell align="left">12:00</CustomTableCell>
            <CustomTableCell align="left">13:00</CustomTableCell>
              <CustomTableCell align="left">14:00</CustomTableCell>
                <CustomTableCell align="left">15:00</CustomTableCell>
                  <CustomTableCell align="left">16:00</CustomTableCell>
                  <CustomTableCell align="left">17:00</CustomTableCell>
                    <CustomTableCell align="left">18:00</CustomTableCell>
                    <CustomTableCell align="left">19:00</CustomTableCell>
            </TableRow>

                    </TableHead>
                            <TableBody>
                                 <TableRow  className={this.props.classes.row}>
                            <CustomTableCell align="left">{}</CustomTableCell>
                            <CustomTableCell align="left">{ }</CustomTableCell>
                                    </TableRow>
                                </TableBody>
                                  </Table>


</div>
       )
     }
     searchUpdated (term) {
     this.setState({searchTerm: term})
     }
   }
    Journal.propTypes = {
       classes: PropTypes.object.isRequired,
     }


export default withStyles(styles)(Books);
