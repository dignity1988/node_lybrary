import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';


const styles = theme => ({
  root: {
    margin: theme.spacing.unit *3,

  },
table: {
    flex: 1
  },
row: {
    marginLeft: -12,
    marginLeft: 20
  }
})

class OutlinedTextFields extends React.Component {

  	constructor(props){
  		super(props);
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
  		this.state = {
      name:"",
      surname:"",
      lastname:"",
      name:"",
      address:"",
      date_of_birth:"",
      occupation:"",
      registration_year:"",
      notes:"",
      passport:"",
      visit:"",
  		}
  	}

    handleChange(e) {
       this.setState({ [e.target.name] : e.target.value });
    }

  	handleSubmit(e) {
  		e.preventDefault();
  		var data = {
        abonement_id: this.state.abonement_id,
  		name: this.state.name,
  		surname: this.state.surname,
  		phone_number: this.state.confirmEmail,
  			password: this.state.password,
        education: this.state.education,
  		}
  		fetch( process.env.PUBLIC_URL + '/users', {
       method: 'POST',
       headers: {'Content-Type':'application/json'},
  			body: JSON.stringify(data)
  		}).then(function(response){
  			if(response.status >= 400) {
  				throw new Error("Bad response from server");
  			}
  			return response.json();
  		}).then(function(data){
  			console.log(data);
  			if(data == "success"){
  				this.setState({sucess: 'User has been successfully registered'});
  			}
  		}).catch(function(err){
  			console.log(err);
  		});
}  		;

  render()
  {
    const { classes } = this.props
    return (
<div>

surname:
name:
lastname:
phone_number
:
date_of_birth:
occupation:
registration_year:
notes:
:
passport:
visit:
        <Paper>
          <form className={classes.container} noValidate autoComplete="off">
          <TextField
            id="outlined-name"
            label="Номер абонементу"
            name="name"
            className={classes.textField}
            onChange={this.handleChange}
            defaultValue={this.state.abonement_id}
            margin="normal"
            variant="outlined"
          />
          <TextField
            id="outlined-surname"
            label="Призвіще"
            name="surname"
            className={classes.textField}
            onChange={this.handleChange}
            defaultValue={this.state.surname}
            margin="normal"
            variant="outlined"
          />
          <TextField
            id="outlined-surname"
            label="Призвіще"
            name="surname"
            className={classes.textField}
            onChange={this.handleChange}
            defaultValue={this.state.name}
            margin="normal"
            variant="outlined"
          />
          <TextField
            id="outlined-surname"
            label="Номер абонементу"
            defaultValue={this.state.lastname}
            name="abonement_id"
            className={classes.textField}
            onChange={this.handleChange}
            margin="normal"
            variant="outlined"
          />
          <TextField
            id="6"
            label="Ностанній візит"
            defaultValue={this.state.date_of_birth}
            name="abonement_id"
            className={classes.textField}
            onChange={this.handleChange}
            margin="normal"
            variant="outlined"
          />
          <TextField
            id="5"
            label=""
            defaultValue={this.state.education}
            name="abonement_id"
            className={classes.textField}
            onChange={this.handleChange}
            margin="normal"
            variant="outlined"
          />
          <TextField
            id="4"
            label="Професія"
  defaultValue={this.state.occupation}
            name="abonement_id"
            className={classes.textField}
            onChange={this.handleChange}
            margin="normal"
            variant="outlined"
          />
          <TextField
            id="outlined-surnam2e"
            label="Ностанній візит"
  defaultValue={this.state.address}
            name="abonement_id"
            className={classes.textField}
            onChange={this.handleChange}
            margin="normal"
            variant="outlined"
          />
          <TextField
            id="outlined-surnam2e"
            label="Останній візит"
  defaultValue={this.state.phone_number}
            name="abonement_id"
            className={classes.textField}
            onChange={this.handleChange}
            margin="normal"
            variant="outlined"
          />
          <TextField
            id="outlined-surnam2e"
            label="Ностанній візит"
  defaultValue={this.state.visit}
            name="abonement_id"
            className={classes.textField}
            onChange={this.handleChange}
            margin="normal"
            variant="outlined"
          />
<Button variant="contained" value="Додати користувача"  onClick={this.handleSubmit} label={"Додати користувача"} >
          Додати користувача
              </Button>
      </form>
    </Paper>
    </div>
    )


}



}
OutlinedTextFields.propTypes = {
   classes: PropTypes.object.isRequired,
 }
export default withStyles(styles)(OutlinedTextFields);
