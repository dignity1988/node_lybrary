import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import SearchInput, {createFilter} from 'react-search-input';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Users from '/../components/List/'
const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    border: theme.palette.common.green,
    padding: 5+'px',
    textAlign: 'left',
  },
  body: {
    fontSize: 14,
      textAlign: 'left',
    padding: 5+'px',
  },
}))(TableCell);

const styles = theme => ({
  root: {
    margin: theme.spacing.unit *3,

  },
table: {
    flex: 1
  },
row: {
    marginLeft: -12,
    marginLeft: 20
  }
})




const KEYS_TO_FILTERS = ['abonement_id','surname'];
var startDate = new Date("2019-01-01"); // YYYY-MM-DD
var endDate = new Date("2020-01-01"); //YYYY-MM-DD

var getDateArray = function(start, end) {
    var arr = new Array();  // arr = [];
    var dt = new Date(start);

    // map()
    while (dt <= end ) {
        if (dt.getDay() != 5) {
          arr.push(new Date(dt));
        }

        dt.setDate(dt.getDate() + 1);
    }

    return arr;
}


 getDateArray();

 getDateArray(startDate, endDate);

function DrawCells(dateArr){
for (let i = 0; i < dateArr.length; i++) {
return(
  <CustomTableCell>
{dateArr[i].getDay()}{dateArr[i].getFullMonth()}
  </CustomTableCell>

);
}
}

class Journal extends React.Component {
  constructor (props) {
  super(props)
  this.state = {
  users:[],
  classes:{},
  searchTerm:'',
  dateArr:[]
  }
   this.searchUpdated = this.searchUpdated.bind(this)
  }
  componentDidMount() {
  fetch( process.env.PUBLIC_URL + '/users')
  .then(response => {
  if(response.ok) return response.json()}
  ).then(json => this.setState({users:json})).then(console.log(this.state.users));
  }

render(){
 const {classes} = this.props;
 /*creating several diferent filters that works separately*/
 const filteredNames = this.state.users.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
 const {dateArr} = this.props;
  return (

    <Paper className={this.props.classes.root}>
    /*trying to import method into the container that is the render method to app js*/
    <List List={this.state.users} />
    <SearchInput placeholder="Номер абонементу" className="search-input" onChange={this.searchUpdated} />
      <Table  className={this.props.classes.table}>
        <TableHead>
          <TableRow  className={this.props.classes.row}>
            <CustomTableCell>#№</CustomTableCell>
            <CustomTableCell  align="left">Им'я</CustomTableCell>
            <CustomTableCell align="left">Призвіще</CustomTableCell>
            <CustomTableCell align="left">По-батькові</CustomTableCell>
            <CustomTableCell align="left">Рік народження</CustomTableCell>
          <CustomTableCell align="left">Освіта</CustomTableCell>
            <CustomTableCell align="left">Пофесія</CustomTableCell>
            <CustomTableCell align="left">Місце роботи</CustomTableCell>
          <CustomTableCell align="left">Домашня адреса</CustomTableCell>
        <CustomTableCell align="left">Телефон</CustomTableCell>
          <CustomTableCell align="left">Рік реєстрації</CustomTableCell>
      <CustomTableCell align="left"></CustomTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {filteredNames.map(key => (
            <TableRow>
              <CustomTableCell component="th" scope="row">
                {key.name}

              </CustomTableCell>
              <CustomTableCell align="left">{key.abonement_id}</CustomTableCell>
              <CustomTableCell align="left">{key.name}</CustomTableCell>
<CustomTableCell align="left">{key.surname}</CustomTableCell>
<CustomTableCell align="left">{key.lastname}</CustomTableCell>
<CustomTableCell align="left">{key.date_of_birth}</CustomTableCell>
<CustomTableCell align="left">{key.education}</CustomTableCell>
<CustomTableCell align="left">{key.occupation}</CustomTableCell>
<CustomTableCell align="left">{key.address}</CustomTableCell>
<CustomTableCell align="left">{key.phone_number}</CustomTableCell>
<CustomTableCell align="left">{key.registration_year}</CustomTableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>

          <Table  className={this.props.classes.table}>
            <TableHead>
            <TableRow  className={this.props.classes.row}>
            <CustomTableCell>#№</CustomTableCell>
            <CustomTableCell align="left">10:00</CustomTableCell>
            <CustomTableCell align="left">11:00</CustomTableCell>
            <CustomTableCell align="left">12:00</CustomTableCell>
            <CustomTableCell align="left">13:00</CustomTableCell>
              <CustomTableCell align="left">14:00</CustomTableCell>
                <CustomTableCell align="left">15:00</CustomTableCell>
                  <CustomTableCell align="left">16:00</CustomTableCell>
                  <CustomTableCell align="left">17:00</CustomTableCell>
                    <CustomTableCell align="left">18:00</CustomTableCell>
                    <CustomTableCell align="left">19:00</CustomTableCell>
            </TableRow>

                    </TableHead>
                            <TableBody>
                                 <TableRow  className={this.props.classes.row}>
                            <CustomTableCell align="left">{}</CustomTableCell>
                            <CustomTableCell align="left">{ }</CustomTableCell>
                                    </TableRow>
                                </TableBody>
                                  </Table>
          </Paper>


       )
     }
     searchUpdated (term) {
     this.setState({searchTerm: term})
     }
   }
    Journal.propTypes = {
       classes: PropTypes.object.isRequired,
     }


export default withStyles(styles)(Journal);
