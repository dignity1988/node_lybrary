function convertToCSV(tableData, delimiter = ',') {
  if (!Array.isArray(tableData) || !tableData.length) {
    throw new Error('Table data should be a non-empty array of column/value rows');
  }

  var colNames = Object.keys(tableData[0]);
  var rows = tableData.map(rowData => {
    var row = [];
    for (var key in rowData) {
      // I noticed that members column is an array, not sure how you handle it
      // but here it joins the members with comma and wraps the whole thing in
      // double quotes
      if (Array.isArray(rowData[key]) && rowData[key].length) {
        row.push('"' + rowData[key].join(',') + '"');
      } else {
        row.push(rowData[key]);
      }
    }
    return row.join(delimiter);
  });
  rows.unshift(colNames.join(delimiter));
  return rows.join("\n")
}

const csv = convertToCSV([
  {"id_group":"1","name":"Visitor","members":[],"total":"0","discount":"0"},
  {"id_group":"2","name":"Guest","members":[],"total":"0","discount":"0"}
])

console.info(csv)
