import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
const config = {

  apiKey: "AIzaSyAoiBosSeh158pXyr-JOmCqOR-wShPq-pY",
      authDomain: "alef-4d335.firebaseapp.com",
      databaseURL: "https://alef-4d335.firebaseio.com",
      projectId: "alef-4d335",
      storageBucket: "alef-4d335.appspot.com",
      messagingSenderId: "85279532557",
      appId: "1:85279532557:web:ba0a8e6edff8be41"
};


firebase.initializeApp(config);
export const auth = firebase.auth();
export const google = new firebase.auth.GoogleAuthProvider();
export const users = firebase.database().ref('/databases/users');
export const facebook = new firebase.auth.FacebookAuthProvider();



export default firebase;
