import React, { Component } from 'react';
import './List.css';
import SearchInput, {createFilter} from 'react-search-input';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import { withStyles } from '@material-ui/core/styles';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
const styles = {
  card: {
    display:'inline-flex',
    minWidth: 275,
    maxWidth: 400,
    width:200,
  },
  media: {
    height: 140,
  },
};




const KEYS_TO_FILTERS = ['abonement_id','surname']
class Search extends Component {
constructor (props) {
super(props)
this.state = {
searchTerm: '',
 name: '',
users:[]
}
this.searchUpdated = this.searchUpdated.bind(this)
}
componentDidMount(){
fetch( process.env.PUBLIC_URL + '/users')
.then(response => {
if(response.ok) return response.json()}
).then(json => this.setState({users:json})).then(console.log(this.state.users));
}

handleChange = event => {
this.setState({ name: event.target.value });
};
render() {

  /*creating several diferent filters that works separately*/
const filteredNames = this.state.users.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
const styles = {
  container: {
  display: 'flex',
  flexWrap: 'wrap',
},
formControl: {
  margin: 10,
},
  card: {
    display:'inline-flex',
    minWidth: 275,
    maxWidth: 400,
    width:200,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
};
var name= filteredNames.slice(0, 1).map(key => (
key.name

)
)

  return (

         <div class="flex">


   <Card className={styles.card}>


 <CardContent>
 <FormControl className={styles.formControl}>

<SearchInput placeholder="Номер абонементу" className="search-input" onChange={this.searchUpdated} />
    <FormHelperText id="component-helper-text">Some important helper text</FormHelperText>
  </FormControl>
  {filteredNames.slice(0, 1).map(key => (
  <div class="">
  <p>{key.name}</p>
  </div>
  )
  )
  }
  {filteredNames.slice(0, 1).map(key => (
  <div class="">
  <p>{key.abonement_id}</p>
  </div>
  )
  )
  }
  {filteredNames.slice(0, 1).map(key => (
  <div class="">
  <p>{key.surname}</p>
  </div>
  )
  )
  }

  {filteredNames.slice(0, 1).map(key => (
  <div class="">
  <p>{key.phone_number}</p>
  </div>
  )
  )
  }

  {filteredNames.slice(0, 1).map(key => (
  <div class="">
  <p>{key.address}</p>
  </div>
  )
  )
  }

  {filteredNames.slice(0, 1).map(key => (
  <div class="">
  <p>{key.date_of_birth}</p>
  </div>
  )
  )
  }

  {filteredNames.slice(0, 1).map(key => (
  <div class="">
  <p>{key.occupation}</p>
  </div>
  )
  )
  }

  {filteredNames.slice(0, 1).map(key => (
  <div class="">
  <p>{key.registration_year}</p>
  </div>
  )
  )
  }

  {filteredNames.slice(0, 1).map(key => (
  <div class="">
  <p>{key.notes}</p>
  </div>
  )
  )
  }

 </CardContent>
    <CardActions>


</CardActions>
</Card>

       )

     }
searchUpdated (term) {
this.setState({searchTerm: term})
}
}
export default Search;
