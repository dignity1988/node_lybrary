import React from 'react';
import Button from '@material-ui/core/Button';
import firebase, { auth, provider } from '../../components/firebase/';

const logoutUser = () => {
 auth.signOut()
     .then(() => {
       this.setState({
         user: null
       });
     });
};
const Logout = () => {
 return (<Button onClick={logoutUser} />);
};
export default Logout;
