import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  dense: {
    marginTop: 16,
  },
  menu: {
    width: 200,
  },
});



class OutlinedTextFields extends React.Component {
  state = {
    name: 'Cat in the Hat',
    age: '',
    multiline: 'Controlled',
    currency: '',
  };
  fetch( process.env.PUBLIC_URL + '/users', {
   method: 'POST',
   headers: {'Content-Type':'application/json'},
   body: {
   abonement_id:,
   surname:,
   lastname:,
   name:,
   phone_number:,
   address:,
   date_of_birth:,
   occupation:,
   registration_year:,
   notes:,
   education:,
   passport:,
   visit:,
   }
  }).then(response => {
    if(response.ok)  return JSON.parse((response))}
  )
  .then(json => this.setState({users:json}))
  .then(function(body) {
        });
  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
      [surname]: event.target.value,
      [abonement_id]:event.target.value,
      [surname]:event.target.value,
      [lastname]:event.target.value,
      [name]:event.target.value,
      [phone_number]:event.target.value,
      [address]:event.target.value,
      [date_of_birth]:event.target.value,
      [occupation]:event.target.value,
      [registration_year]:event.target.value,
      [notes]:event.target.value,
      [education]:event.target.value,
      [passport]:event.target.value,
      [visit]:event.target.value,
    });
  };

  render() {
    const { classes } = this.props;
    return (
        <form method="POST" className={classes.container} noValidate autoComplete="off">
          <TextField
            id="outlined-name"
            label="Name"
            className={classes.textField}
            value={this.state.name}
            onChange={this.handleChange('name')}
            margin="normal"
            variant="outlined"
          />
      </form>
    );
  }
}

OutlinedTextFields.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(OutlinedTextFields);
