import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Intro from './components/Intro';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Home from './components/home/index.js';
import PrivateRoute  from './components/PrivateRoute/index.js';
import firebase, { auth, provider, google, facebook, users } from './components/firebase/index.js';
import Journal from './components/journal';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import OutlinedTextFields from './components/Form';
import Navbar from './components/navbar';
import SignUp from "./components/SignUp";
import Login from "./components/login";
import Logout from "./components/logout";
import { BrowserRouter as Router, Route } from 'react-router-dom';
const styles = theme => ({
root: {
flexGrow: 1,
margin:'auto',
padding:20+'px',
maxWidth:1920+'px',
width:80+'%',
display: 'flex',
float: 'none',
flexDirection: 'column',
},
paper: {
textAlign: 'center',
padding: theme.spacing.unit * 3,
textAlign: 'center',
color: theme.palette.text.secondary,
},
grid: {
textAlign: 'center',
margin:'auto',
alignItems:'center',
width:'100%',
color: theme.palette.text.secondary,
},
});

class App extends Component {


constructor (props) {
super(props)
this.state = {
value: 0, loading:true, authentificated: false, user: null,
}
this.login = this.login_google.bind(this);
  this.logout = this.logout.bind(this);
}
handleChange = (event, value) => {
this.setState({ value });
};
/*
login() {
  auth.signInWithEmailAndPassword(email, password).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    // ...
  });
  }
  */
login_google() {
auth.signInWithPopup(google).then(function(result) {
  // This gives you a Facebook Access Token. You can use it to access the Facebook API.
  var token = result.credential.accessToken;
  // The signed-in user info.
  var user = result.user;
  alert('Welcome', user)
  // ...
}).catch(function(error) {
  // Handle Errors here.
  var errorCode = error.code;
  var errorMessage = error.message;
  // The email of the user's account used.
  var email = error.email;
  // The firebase.auth.AuthCredential type that was used.
  var credential = error.credential;
  alert(error.message)
});
  }
  login_facebook() {
  auth.signInWithPopup(google).then(function(result) {
    // This gives you a Facebook Access Token. You can use it to access the Facebook API.
    var token = result.credential.accessToken;
    // The signed-in user info.
    var user = result.user;
    alert('Welcome', user)
    // ...
  }).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    // The email of the user's account used.
    var email = error.email;
    // The firebase.auth.AuthCredential type that was used.
    var credential = error.credential;
    alert(error.message)
  });
    }

logout() {
  try {
    auth.signOut()
        .then(() => {
          this.setState({
            user: null
          });
        }).then(console.log('user signed out'));
  } catch (e) {
    console.error(e.message || "error signing user out.");
  }

}

componentDidMount() {
  auth.onAuthStateChanged((user) => {
    if (user) {
      this.setState({currentUser: user, authenticated: true});
    }
    else {
    this.setState({
    authenticated: false,
    currentUser: null,
    loading: false
    });
    }
  })

}
componentWillMount() {
auth.onAuthStateChanged(user => {
if (user) {
this.setState({
authenticated: true,
currentUser: user,
loading: false
})
} else {
this.setState({
authenticated: false,
currentUser: null,
loading: false
});
}
});
}

render() {
const {classes} = this.props;
const { authenticated, loading } = this.state;
let loader;
{if (this.state.loading){
loader= <div>loading</div>}
}

return (
    <Router>

    <Grid>

      <Paper className={this.props.classes.root} >
<SignUp />

      {this.state.currentUser ?
                  <button onClick={this.logout}>Logout</button>
                :
                  <button onClick={this.login_google}>Log In</button>

                }
{this.state.currentUser ?
  <Navbar />
  :
  <div>Please authentificate to enter search the books</div>
}

      <PrivateRoute exact path="/" component={Home} authenticated={authenticated} />
      <Route exact path="/login" component={Login} />
            <Route exact path="/logout" component={Logout} />

      <Route exact path="/signup" component={SignUp} />
      <Route exact path="/signup" component={SignUp} />
      <Grid container spacing={24} justify="center" className={classes.Grid}>

          <Grid item xs={5}>
            <Paper className={this.props.classes.paper}>
            </Paper>
          </Grid>
          <Grid item xs={5}>
          <Paper className={classes.paper}>
            <OutlinedTextFields />
                  </Paper>
          </Grid>
      </Grid>
      </Paper>
    </Grid>
  </Router>
  );
}

  }



  App.propTypes = {
  classes: PropTypes.object.isRequired};


  export default withStyles(styles)(App);
