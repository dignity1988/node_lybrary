/*const {google} = require('googleapis');

const book =  google.books.volumes.list({
  q: 'query',
  fields: 'items(id,volumeInfo(authors,title))'
})
*/
const request = require("request");
const util = require('util');
const requestPromise = util.promisify(request);

bookimg = function(query,res)  {

try {
       response =  await requestPromise('https://www.googleapis.com/books/v1/volumes/', {method: "GET",
    headers: {
        "content-type": "application/json",
      },qs :{
        'q.title': query.title,
              'q.author': query.author,

        fields: 'items(volumeInfo( imageLinks(thumbnail)))'
      }
    });

} catch (error) {
    console.error(error);

}
console.log(response);
let json=JSON.parse(response.body);
res=json.items[0].volumeInfo.imageLinks.thumbnail;
console.log(json.items[0].volumeInfo.imageLinks.thumbnail)
return res;
}
bookimg('Солярис');
