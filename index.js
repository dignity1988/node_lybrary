const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());
const path = require('path');
const mongoose = require('mongoose');
require('dotenv').config({ path: '.env' });

/* application actions to use database action with users router*/
require('./users.router.js')(app);
require('./books.router.js')(app);
const PORT = process.env.PORT ||5000;
app.use(express.static(path.join(__dirname, 'build/')));
// if you need api routes add them here

/*
const path = require('./books.search.js');
*/


app.listen(PORT, () => {
  console.log(`Check out the app at http://localhost:${PORT}`);
});
//creating express routes for CRUD database
mongoose.set('useCreateIndex', true);
mongoose.connect('mongodb://localhost:27017/database',{ useNewUrlParser: true } );
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('hooray,local connection established');
  // we're connected!
});

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


/*creating login route*/
app.post('/login',(req,res)=>{
    var message;
    for(var user of users){
      if(user.name!=req.body.name){
          message="Wrong Name";
      }else{
          if(user.password!=req.body.password){
              message="Wrong Password";
              break;
          }
          else{
              message="Login Successful";
              break;
          }
      }
    }
    res.send(message);
});
app.use(function(err, req, res, next) {
  // Do logging and user-friendly error message display
  console.error(err);
  res.status(500).send('internal server error');
  next();
})


if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
     });
 }
app.get('/', function (req, res, next) {
  Promise.resolve().then((req, res) => {
    res.sendFile(path.resolve(__dirname, 'build', 'index.html'),
    function (err, data) {
          res.locals.index = data
          next(err)
        });



  }).catch(next) // Errors will be passed to Express.
})



/*
import db from '../models'
import { createJWToken } from '../libs/auth'
import { paramCheck } from '../middlewares'

const router = express.Router()


  ROUTES


router.post('*', paramCheck(['email', 'password']))

router.post('/login', (req, res) =>
{
  let { email, password } = req.body

  db.User.findByEmail(email)
  .then((user) => (!user) ? Promise.reject("User not found.") : user)
  .then((user) => user.comparePassword(password))
  .then((user) => user.publicParse(user))
  .then((user) =>
  {
    res.status(200)
      .json({
        success: true,
        token: createJWToken({
            sessionData: user,
            maxAge: 3600
          })
      })
  })
  .catch((err) =>
  {
    res.status(401)
      .json({
        message: err || "Validation failed. Given email and password aren't matching."
      })
  })
})

export default router








const config = require('./config.js');


// delete him
user.remove(function(err) {
  if (err) throw err;

  console.log('User successfully deleted!');
});
});












*/











/*creating routes to separate routes to post and get users from frontend of the application*/
/*creating */
app.get('/users', (req, res) => {
  res.send('text');
});
