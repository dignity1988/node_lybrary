
import React, { Component } from "react";
import { StyleSheet, Text, View, TextInput, Button, ScrollView } from "react-native";
import {Home} from "./components/pages/home.js";
import {Login} from "./components/pages/login.js";
import {Search} from "./components/pages/Search.js";
import {Page} from "./components/page.js";
export default class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      bookName: "Шевченко",
      books: [],
      isLoading: false
  }
}
  bookNameChangedHandler = val => {
    this.setState({
      bookName: val
    });
  };

  bookSubmitHandler = () => {

      return fetch('https://catalogcdl.herokuapp.com/'+'search_books?term=' + this.state.bookName)
        .then((response) => response.json())
        .then((responseJson) => {

          this.setState({
            isLoading: false,
            books: responseJson,
          }, function(){
console.log( responseJson)
          });

        })
        .catch((error) =>{
          console.error(error);
        });
    }




    componentDidMount(){
      fetch('https://catalogcdl.herokuapp.com/'+'search_books?term=' + this.state.bookName)
        .then((response) => response.json())
        .then((responseJson) => {

          this.setState({
            isLoading: false,
            books: responseJson,
          }, function(){
      console.log( responseJson)
          });

        })
        .catch((error) =>{
          console.error(error);
        });

    }
  render() {
    const booksOutput = this.state.books.map(key => (
                      <Text>{key.name}</Text>

      ));


    return (
      <ScrollView  contentContainerStyle={styles.container}>
        <View style={styles.inputContainer}>
          <TextInput
            bookholder="An awesome book"
            value={this.state.bookName}
            onChangeText={this.bookNameChangedHandler}
            style={styles.bookInput}
          />
          <Button
            title="Search"
            style={styles.bookButton}
            onPress={this.bookSubmitHandler}
          />

      {booksOutput}
      </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 26,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  inputContainer: {
    // flex: 1,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  placeInput: {
    width: "70%"
  },
  placeButton: {
    width: "30%"
  },
  listContainer: {
    width: "100%"
  }
});
